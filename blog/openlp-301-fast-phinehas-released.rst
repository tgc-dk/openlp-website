.. title: OpenLP 3.0.1 "Fast Phinehas" Released
.. slug: blog/2023/01/13/openlp-301-fast-phinehas-released
.. date: 2023-01-13 19:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-301-fast-phinehas-released.jpg

As we said in our `previous announcement`_, we are moving to rolling out fixes and updates faster. So here, hot in the heels of our 3.0 release, is version 3.0.1 which contains a number of fixes for issues you may have experienced.

Bugs Fixed
----------

* Fix theme loading of presentations 
* Fix a bug when migrating 2.4.6 presentations to 3.0
* Fix song ordering for certain languages (On Linux this introduces a dependency on PyICU)
* Fix an error when exporting song reporting
* Fix the remote version test 
* Fix Print Service missing newlines on print 
* Video volume and repeat settings are now saved consistently between media. 

Known Issues
------------

.. warning:: **Ubuntu 22.04, Linux Mint**

   The main view in the web remote does not update automatically on Ubuntu 22.04 due to
   Ubuntu/Mint shipping an old version of python3-websockets. This can be fixed by
   downloading and installing an updated version of the package from `the official
   Ubuntu archive`_.

.. warning:: **Wayland on Linux**

   OpenLP at present does not behave well under Wayland so the recommendation is to run
   under X11. If you can't run in X11 (or prefer to run it in XWayland), you should
   start it with ``QT_QPA_PLATFORM`` environment variable set to ``xcb``, although the
   Main View from the Web Remote will not work.

.. warning:: **Video backgrounds**

   Video / streaming backgrounds and background audio in songs currently do not work
   together. You may experience either the video not working but the audio works, or
   the audio doesn't work while the video does.

.. warning:: **macOS**

   macOS may report OpenLP as damaged. If you encounter this problem, you can try the
   following solution:
       1. From Applications / Utilities run Terminal
       2. In Terminal, type in the following command::
           
           xattr -dr com.apple.quarantine /Applications/OpenLP.app
           
       3. This should allow you to start OpenLP normally

.. warning:: **macOS on Apple Silicon Chips**

   The VLC integration does not work.

   Some people have reported success when downloading and installing the **Universal Binary** from
   the `VLC website`_, but we have not confirmed this.

Download
--------

Head on over to the `downloads section of the website`_ to download version 3.0.1 now!

.. note:: **Linux users**

   Please note that distributions can take a few days to be updated. Please be patient
   while the packagers update the packages.

.. _previous announcement: https://openlp.org/blog/2022/12/31/openlp-30-steadfast-simeon-released
.. _the official Ubuntu archive: http://archive.ubuntu.com/ubuntu/pool/universe/p/python-websockets/python3-websockets_10.2-1_all.deb
.. _VLC website: https://www.videolan.org/vlc/download-macosx.html
.. _downloads section of the website: https://openlp.org/#downloads
